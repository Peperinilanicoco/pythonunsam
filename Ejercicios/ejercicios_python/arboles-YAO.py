# -*- coding: utf-8 -*-
"""
Created on Mon Aug 17 00:01:43 2020

@author: User
"""

import os
import csv 

"""
Ejercicio 2.22: Lectura de los árboles de un parque
Definí una función leer_parque(nombre_archivo, parque) que abra el archivo
 indicado y devuelva una lista de diccionarios con la información del 
 parque especificado. La función debe devolver, en una lista un diccionario
 con todos los datos por cada árbol del parque elegido (recordá que cada fila
 del csv es un árbol).

Sugerencia: basate en la función leer_camion() y usá también el comando zip
 como hiciste en el Ejercicio 2.19 para combinar el encabezado del archivo 
 con los datos de cada fila. Inicialmente no te preocupes por los tipos de 
 datos de cada columna, pero cuando empieces a operar con una columna modifica
 esta función para que ese dato sea del tipo adecuado para operar.

Observación: La columna que indica el nombre del parque en el que se
encuentra el árbol se llama 'espacio_ve' en el archivo CSV.

Probá con el parque "GENERAL PAZ" para tener un ejemplo de trabajo,
debería darte una lista con 690 árboles.
"""

def leer_parque(nombre_archivo, parque):
    datos = []
    with open(nombre_archivo, "rt") as f: #Abro el archivo como f
            filas = csv.reader(f)   #devuelve un interador para iterar sobre las lineas del archivo CSV
            encabezados = next(filas) #Saltea la fila del archivo que contiene los nombres de las columnas
            fila = next(filas)
            #print(encabezados)
            #print(fila)
            
            for nro_fila, fila in enumerate(filas, start=1):
                try:
                    record = dict(zip(encabezados, fila)) #Genero la lista con 1 diccionario x arbol.
                    if record["espacio_ve"]==parque: #Filtro por parque: dejo que appendee si y solo si espacio_ve == parque [CREDITO: tip de Rafel Grimson, Slack clase 2]
                        datos.append(record)
                except ValueError:   
                    print("excepcion de tipo ValueError atrapada")    
                    next
   
    
   #Antes de devolver 'datos', seria ideal que cada variable sea de su tipo.
    for diccionarios in datos: 
        for clave in diccionarios: 
            try:
                diccionarios["long"] = float(diccionarios["long"]) 
                diccionarios["lat"] = float(diccionarios["lat"]) 
                diccionarios["altura_tot"] = float(diccionarios["altura_tot"]) 
                diccionarios["diametro"] = float(diccionarios["diametro"]) 
                diccionarios["inclinacio"] = float(diccionarios["inclinacio"]) 
                #etc...
            except ValueError:
                next
    
    return datos


#Variables que permiten modificar lo que se quiere de cada funcion
#como especie, el parque leido o un conjunto de parques a analizar.
parque = "SAAVEDRA LAMAS, CARLOS, Dr."
nombre_archivo = "Data/arbolado-en-espacios-verdes.csv"
lista_arboles = leer_parque("Data/arbolado-en-espacios-verdes.csv", parque)
len(lista_arboles)
parquesDeInteres = ["ANDES, LOS", "CENTENARIO", "GENERAL PAZ"]
especie = "Jacarandá"

"""
Ejercicio 2.23: Determinar las especies en un parque
Escribí una función especies(lista_arboles) que tome una lista de árboles como 
la generada en el ejercicio anterior y devuelva el conjunto de especies
(la columna 'nombre_com' del archivo) que figuran en la lista.
"""

def especies(lista_arboles):
    especies = [] #Lista vacia
    for arbol in range(len(lista_arboles)):
        especie = lista_arboles[arbol]["nombre_com"]
        especies.append(especie)
    especies = set(especies)  #lo convierte en conjunto, eliminando duplicados
    return especies

especiesEncontradas = especies(lista_arboles) #Hay 81 sp encontradas en el parque.
len(especiesEncontradas)# Hay 81 sp

"""
Ejercicio 2.24: Contar ejemplares por especie
Usando contadores como en el Ejercicio 2.21, escribí una función 
contar_ejemplares(lista_arboles) que, dada una lista como la que generada con 
leer_parque(), devuelva un diccionario en el que las especies (recordá, es la 
columna 'nombre_com' del archivo) sean las claves y tengan como valores 
asociados la cantidad de ejemplares en esa especie en la lista dada.

Luego, combiná esta función con leer_parque() y con el método most_common()
 para informar las cinco especies más frecuentes en cada uno de los siguientes
 parques:

'GENERAL PAZ'
'ANDES, LOS'
'CENTENARIO'
"""
def contar_ejemplares(lista_arboles):
    from collections import Counter
    ejemplares_x_especie = Counter()
    
    #Inspiracion de https://stackoverflow.com/questions/3496518/using-a-dictionary-to-count-the-items-in-a-list
    #desgraciadamente no pude aplicar directo lo que vimos en clase asi que lo resolvi asi
    nombreEspecies = []
    for especie in lista_arboles: 
        nombreEspecies.append(especie["nombre_com"])
        ejemplares_x_especie = Counter(nombreEspecies)    
     
    return ejemplares_x_especie.most_common(5) #devuelve los 5 arboles mas representados.
    
ranking_especies_mas_comunes = contar_ejemplares(lista_arboles)


#Programa pedido por el ejercicio 2.24:
for parque in range(len(parquesDeInteres)):
    lista_arboles = leer_parque("Data/arbolado-en-espacios-verdes.csv", parquesDeInteres[parque])
    ranking_especies_mas_comunes = contar_ejemplares(lista_arboles)
    print("El ranking de arboles para el parque", parquesDeInteres[parque], "es :", ranking_especies_mas_comunes,".\n")

"""
Ejercicio 2.25: Alturas de una especie en una lista
Escribí una función obtener_alturas(lista_arboles, especie) que, dada una lista 
de árboles como la anterior y una especie de árbol (un valor de la columna
'nombre_com' del archivo), devuelva una lista con las alturas 
(columna 'altura_tot') de los ejemplares de esa especie en la lista.

Observación: Acá sí, fijate de devolver las alturas como números (de 
punto flotante) y no como cadenas de caracteres. Podés hacer esto modificando
 leer_parque.

Usala para calcular la altura promedio y altura máxima de los 'Jacarandá' en
 los tres parques mencionados:
"""

def obtener_alturas(lista_arboles, especie):
    listaDeAlturas = [] #lista vacia a ser usada por la funcion
    for arbol in lista_arboles:
        if arbol["nombre_com"] == especie:
            listaDeAlturas.append(arbol["altura_tot"])      
    
    return listaDeAlturas

listaDeAlturas = obtener_alturas(lista_arboles, especie)    


#Programa pedido por el ejercicio 2.25:

#obs : para la media  se puede hacer  sum(listaDeAlturas) / len(listaDeAlturas)
# Yo originalmente use np.mean
import numpy as np 
for i in range(len(parquesDeInteres)):
    lista_arboles = leer_parque("Data/arbolado-en-espacios-verdes.csv", parquesDeInteres[i])  #obtengo el parque i
    listaDeAlturas = obtener_alturas(lista_arboles, especie)     
    alturaMedia = np.mean(listaDeAlturas)
    alturaMaxima = max(listaDeAlturas)
    print(" Para la especie", especie, "en el parque", parquesDeInteres[i], "la altura promedio es:", round(alturaMedia,3), "y su la maxima es: ", alturaMaxima,".\n")


"""Ejercicio 2.26
Escribí una función obtener_inclinaciones(lista_arboles, especie) que, dada una
 especie de árbol y una lista de árboles como la anterior, devuelva una lista
 con las inclinaciones (columna 'inclinacio') de los ejemplares de esa especie.
""" 
especie = "Falso Guayabo (Guayaba del Brasil)"
parque = "CENTENARIO"
lista_arboles = leer_parque(nombre_archivo, parque)

def obtener_inclinaciones(lista_arboles, especie):
    listaDeInclinaciones = []
    for arbol in lista_arboles:
        if arbol["nombre_com"] == especie:
            listaDeInclinaciones.append(arbol["inclinacio"])

    return listaDeInclinaciones

listaDeInclinaciones = obtener_inclinaciones(lista_arboles, especie)
max(listaDeInclinaciones)


"""
Ejercicio 2.27: Especie con el ejemplar más inclinado
Combinando la función especies() con obtener_inclinaciones() escribí una función 
especimen_mas_inclinado(lista_arboles) que, dada una lista de árboles 
devuelva la especie que tiene el ejemplar más inclinado y su inclinación.

Correlo para los tres parques mencionados anteriormente.

Resultados. Deberías obtener, por ejemplo, que en el Parque Centenario hay un
Falso Guayabo inclinado 80 grados.
"""

#OBS. piden que la funcion dependa de lista_arboles, no de especie ni de la salida.

def especimen_mas_inclinado(lista_arboles):
    lista_de_inclinaciones = []
    for arbol in lista_arboles:
        lista_de_inclinaciones.append(arbol["inclinacio"])
    
    for arbol in lista_arboles:
        if arbol["inclinacio"] == max(lista_de_inclinaciones):
            out = (arbol["nombre_com"], max(lista_de_inclinaciones))

    return out

#corriendolo par alos 3 parques...
for parque in range(len(parquesDeInteres)):
    lista_arboles = leer_parque("Data/arbolado-en-espacios-verdes.csv", parquesDeInteres[parque])
    inclinacion_maxima = especimen_mas_inclinado(lista_arboles)
    print("El parque", parquesDeInteres[parque], " tiene un ", inclinacion_maxima[0], "de inclinacion: ", inclinacion_maxima[1]," grados.\n")


"""
Ejercicio 2.28: Especie con más inclinada en promedio
Volvé a combinar las funciones anteriores para escribir la función 
especie_promedio_mas_inclinada(lista_arboles) que, dada una lista de árboles 
devuelva la especie que en promedio tiene la mayor inclinación y el promedio 
calculado..

Resultados. Deberías obtener, por ejemplo, que los Álamos Plateados del Parque 
Los Andes tiene un promedio de inclinación de 25 grados.
"""


def especie_promedio_mas_inclinada(lista_arboles):
    lista_de_promedios = []
    for especie in especiesEncontradas:
        if especie in lista_de_promedios: #le pido que se saltee la especie si ya se encontro con ella una vez en especiesEncontradas
            next    
        else:
            lista_de_inclinaciones = obtener_inclinaciones(lista_arboles, especie)
            inc_promedio = np.mean(lista_de_inclinaciones)
            tupla = inc_promedio, especie
            lista_de_promedios.append(tupla)
    
    print(max(lista_de_promedios))        
            
    
"""
Preguntas extras: ¿Qué habría que cambiar para obtener la especie con un ejemplar
más inclinado de toda la ciudad y no solo de un parque? ¿Podrías dar la latitud
y longitud de ese ejemplar? ¿Y dónde se encuentra (lat,lon) el ejemplar más alto? 
¿De qué especie es?           
"""
            
nombre_archivo = "Data/arbolado-en-espacios-verdes.csv"

def leer_ciudad(nombre_archivo):
    datos = []
    with open(nombre_archivo, "rt", encoding="utf8") as f: #Abro el archivo como f
            filas = csv.reader(f)   #devuelve un interador para iterar sobre las lineas del archivo CSV
            encabezados = next(filas) #Saltea la fila del archivo que contiene los nombres de las columnas
            fila = next(filas)
                 
            for nro_fila, fila in enumerate(filas, start=1):
                try:
                    record = dict(zip(encabezados, fila)) #Genero la lista con 1 diccionario x arbol.
                    datos.append(record)
                except ValueError:   
                    print("excepcion de tipo ValueError atrapada")    
                    next
   
    
   #Antes de devolver 'datos', seria ideal que cada variable sea de su tipo.
    for diccionarios in datos: 
        for clave in diccionarios: 
            try:
                diccionarios["long"] = float(diccionarios["long"]) 
                diccionarios["lat"] = float(diccionarios["lat"]) 
                diccionarios["altura_tot"] = float(diccionarios["altura_tot"]) 
                diccionarios["diametro"] = float(diccionarios["diametro"]) 
                diccionarios["inclinacio"] = float(diccionarios["inclinacio"]) 
                #etc...
            except ValueError:
                next
    
    return datos


lista_ciudad = leer_ciudad(nombre_archivo)
           

def especimen_mas_inclinado_ciudad(lista_ciudad):
    lista_de_inclinaciones = []
    for arbol in lista_ciudad:
        lista_de_inclinaciones.append(arbol["inclinacio"])
    
    for arbol in lista_ciudad:
        if arbol["inclinacio"] == max(lista_de_inclinaciones):
            out = (arbol["nombre_com"], max(lista_de_inclinaciones))

    return out

mas_inclinado_ciudad = especimen_mas_inclinado_ciudad(lista_ciudad)
print(mas_inclinado_ciudad)



#%%
"""
3.6 Arbolado porteño y comprensión de listas

"""


"""
Ejercicio 3.18: Lectura de todos los árboles
Basándote en la función leer_parque(nombre_archivo, parque) del Ejercicio 2.22, 
escribí otra leer_arboles(nombre_archivo) que lea el archivo indicado y devuelva una 
lista de diccionarios con la información de todos los árboles en el archivo. La función 
debe devolver una lista conteniendo un diccionario por cada árbol con todos los datos.

Vamos a llamar arboleda a esta lista.
"""

nombre_archivo = "arbolado-en-espacios-verdes.csv"

def leer_arboles(nombre_archivo):
    datos = []
    with open(nombre_archivo, "rt", encoding="utf8") as f: #Abro el archivo como f
            filas = csv.reader(f)   #devuelve un interador para iterar sobre las lineas del archivo CSV
            encabezados = next(filas) #Saltea la fila del archivo que contiene los nombres de las columnas
            fila = next(filas)
                 
            for nro_fila, fila in enumerate(filas, start=1):
                try:
                    record = dict(zip(encabezados, fila)) #Genero la lista con 1 diccionario x arbol.
                    datos.append(record)
                except ValueError:   
                    print("excepcion de tipo ValueError atrapada")    
                    next
   #Antes de devolver 'datos', seria ideal que cada variable sea de su tipo.
    for diccionarios in datos: 
        for clave in diccionarios: 
            try:
                diccionarios["long"] = float(diccionarios["long"]) 
                diccionarios["lat"] = float(diccionarios["lat"]) 
                diccionarios["altura_tot"] = float(diccionarios["altura_tot"]) 
                diccionarios["diametro"] = float(diccionarios["diametro"]) 
                diccionarios["inclinacio"] = float(diccionarios["inclinacio"]) 
                #etc...
            except ValueError:
                next
    
    return datos

arboleda = leer_arboles(nombre_archivo)
           
"""
Ejercicio 3.19: Lista de altos de Jacarandá
Usando comprensión de listas y la variable arboleda podés por ejemplo armar la lista de 
la altura de los árboles.
"""

H=[float(arbol['altura_tot']) for arbol in arboleda if arbol["nombre_com"]=="Jacarandá"]


"""
Ejercicio 3.20: Lista de altos y diámetros de Jacarandá
En el ejercicio anterior usaste una sola linea para seleccionar las alturas de los
Jacarandás en parques porteños. Ahora te proponemos que armes una nueva lista que
tenga pares (tuplas de longitud 2) conteniendo no solo el alto sino también el 
diámetro de cada Jacarandá en la lista.

Esperamos que obtengas una lista similar a esta:
[(5.0, 10.0),
 (5.0, 10.0),
 ...
 (12.0, 25.0),
 ...
 (7.0, 97.0), 
 (8.0, 28.0), 
 (2.0, 30.0), 
 (3.0, 10.0), 
 (17.0, 40.0)]
"""
D=[float(arbol['diametro']) for arbol in arboleda if arbol["nombre_com"]=="Jacarandá"]

listaDeTuplas = list(zip(H, D))
listaDeTuplas

"""
Ejercicio 3.21: Diccionario con medidas
En este ejercicio vamos a considerar algunas especies de árboles. Por ejemplo:
especies = ['Eucalipto', 'Palo borracho rosado', 'Jacarandá']

Te pedimos que armes un diccionario en el que estas especies sean las claves y
los valores asociados sean los datos que generaste en el ejercicio anterior. 
Más aún, organizá tu código dentro de una función 
medidas_de_especies(especies,arboleda) que recibe una lista de nombres de 
especies y una lista como la del Ejercicio 3.18 y devuelve un diccionario cuyas 
claves son estas especies y sus valores asociados sean las medidas generadas 
en el ejercicio anterior.

Vamos a usar esta función la semana próxima. A modo de control, si llamás a la 
función con las tres especies del ejemplo como parámetro (['Eucalipto', 
'Palo borracho rosado', 'Jacarandá']) y la arboleda entera, deberías recibir 
un diccionario con tres entradas (una por especie), cada una con una lista 
asociada conteniendo 4112, 3150 y 3255 pares de números (altos y diámetros), 
respectivamente.

Acordate de guardar los ejercicios de esta sección en el archivo arboles.py.

Extra: casi todes usan un for para crear este diccionario. ¿Lo podés hacer 
usando una comprensión de diccionarios? Te recordamos la sintaxis: 
diccionario = { clave: valor for clave in claves}"""

especies = ['Eucalipto', 'Palo borracho rosado', 'Jacarandá']

def medidas_de_especies(arboleda, especies):
    diccEspecies = {}  #Dicc. vacio
    for especie in especies:
        H=[float(arbol['altura_tot']) for arbol in arboleda if arbol["nombre_com"]==especie]
        D=[float(arbol['diametro']) for arbol in arboleda if arbol["nombre_com"]==especie]
        listaDeTuplas = list(zip(H, D))
        diccEspecies.update({especie:listaDeTuplas}) #CREDITO Stackoverflow: usan .update en dict como analogo a .append en lists.
    

    return diccEspecies

out = medidas_de_especies(arboleda, especies)

#Obs. no lo pude resolver como dict comprehension. 

#%%
import matplotlib as plt
os.path.join('Data', 'arbolado-en-espacios-verdes.csv')
arboleda = leer_arboles(nombre_archivo)
