#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 16:35:56 2020

@author: mauro
"""

def division(dividendo, divisor):
    '''Cálculo de la división
    Pre: Recibe 2 números, divisor debe ser distito de 0.
    Pos: Devuelve un número real, con el cociente de ambos.
    '''
    # assert divisor != 0, "El divisor no puede ser 0"
    return dividendo/divisor


def sumar_enteros(desde, hasta):
    '''Calcula la sumatoria de los números entre desde y hasta.
       Si hasta < desde, entonces devuelve cero.

    Pre: desde y hasta son números enteros
    Pos: Se devuelve el valor de sumar todos los números del intervalo
        [desde, hasta]. Si el intervalo es vacío se devuelve 0
        '''
    sumatoria = 0
    i = desde
    while i<=hasta:
        sumatoria += i
        i += 1
    return sumatoria


def valor_absoluto(n):
    '''Recibe un número y devuelve su valor absoluto.
    Pre: n debe ser un número real
    Pos: Devuelve el valor absoluto de n
    '''
    if n>=0:
        return n
    else:
        return -n


def suma_pares(l):
    '''Recibe una lista de números y devuelve la suma de los números pares 
    que contenga dicha lista.
    Pre: l debe ser una lista de números enteros
    Pos: devuelve la suma de los números pares que contenga la lista. Si no
    hay numeros pares en la lista, devuelve 0
    '''
    res = 0
    for e in l:
        if e%2 == 0:
            res +=e
        else:
            res +=0
    return res


def veces(a, b):
    '''Devuelve el resultado del producto a*b.
    Pre: a debe ser un número real. b debe ser un numero entero positivo
    Pos: devuelve el resultado de multiplicar a*b.
    '''
    #b de hecho puede tomar cualquier valor negativo o positivo, pero si es
    #negativo el ciclo se vuelve infinito y el programa crashea.
    #por eso definí que b debe ser entero positivo.
    res = 0
    nb = b
    while nb !=0:
        res +=a
        nb -= 1


def collatz(n):
    '''Devuelve la longitud de la secuencia de Collatz que se obtiene a partir
    del valor n.
    Pre: n debe ser un numero entero positivo
    Pos: Devuelve la longitud de la secuencia de Collatz.
    '''
    res = 1
    while n != 1:
        if n%2 == 0:
            n = n//2
        else:
            n = 3*n+1
            res +=1
    return res
