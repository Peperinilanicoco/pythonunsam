#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 14:28:10 2020

@author: mauro
"""
#%%
# Ejercicio 3.1: Semántica
# ¿Anda bien en todos los casos de testeo?
#Comento el codigo original para que no se ejecute
# def tiene_a(expresion):
#     n = len(expresion)
#     i = 0
#     while i<n:
#         if expresion[i] == 'a':
#             print(i)
#             return True
#         else:
#             print(i)
#             return False
#         i += 1


#no funciona tal como está. la funcion evalúa solo la primera letra 
# (la posición 0) y como inmediatamente tiene el return, no recorre las demas
# posiciones. Lo comprobé poniendo un print(i) luego de cada condicional.
#Tampoco evalua mayuculas, pero no puedo saber si eso era lo que esperaba o 
#no de la funcion, así q eso no voy a corregir. Para corregir y permitir que
# el programa recorra todos los valores, puse el return False afuera del ciclo
#while, como sigue:

def tiene_a(expresion):
    n = len(expresion)
    i = 0
    while i<n:
        if expresion[i] == 'a':
            print(i)
            return True
        else:
            print(i)
        i += 1
    return False

tiene_a('UNSAM 2020')
tiene_a('abracadabra')
tiene_a('La novela 1984 de George Orwell')
#%%
# Ejercicio 3.2: Sintaxis
# ¿Anda bien en todos los casos de testeo?

# def tiene_a(expresion)
#     n = len(expresion)
#     i = 0
#     while i<n
#         if expresion[i] = 'a'
#             return True
#         i += 1
#     return Falso

# tiene_a('UNSAM 2020')
# tiene_a('La novela 1984 de George Orwell')
#-------------------------------------------------------------------
#Da error de sintaxis así como está. No tiene el ":" luego de la definicion
# de la funcion o de los condicionales. La expresión comparativa 
# expresión[i] = 'a' intenta asignar el valor 'a' y no compararla, y eso no es
# compatible con if. Corresponde '=='. Por ultimo, no exista la expresion 'Falso"
# y lo toma como variable, por lo q dice que no está definida

def tiene_a(expresion):
    n = len(expresion)
    i = 0
    while i<n:
        if expresion[i] == 'a':
            return True
        i += 1
    return False

tiene_a('UNSAM 2020')
tiene_a('La novela 1984 de George Orwell')

#%%
# Ejercicio 3.3: Tipos
# ¿Anda bien en todos los casos de testeo?

# def tiene_uno(expresion):
#     n = len(expresion)
#     i = 0
#     tiene = False
#     while (i<n) and not tiene:
#         if expresion[i] == '1':
#             tiene = True
#         i += 1
#     return tiene


# tiene_uno('UNSAM 2020')
# tiene_uno('La novela 1984 de George Orwell')
# tiene_uno(1984)

# El traceback dice que el objeto de tipo 'entero' no tiene longitud (len()).
# Quiere decir que si el input es un numero entero o float no va a correr el 
# programa. Mi sugerencia sería transformar el input en cadena para que funcione:
    
def tiene_uno(expresion):
    expresion = str(expresion)
    n = len(expresion)
    print(n)
   
    i = 0
    tiene = False
    while (i<n) and not tiene:
        if expresion[i] == '1':
            tiene = True
        i += 1
    return tiene

#%%
# Ejercicio 3.4: Alcances
# La siguiente suma no da lo que debería:

# def suma(a,b):
#     c = a + b
    
# a = 2
# b = 3
# c = suma(a,b)
# print(f"La suma da {a} + {b} = {c}")

# La suma no da lo que debera porque la funcion suma(a,b) no tiene salida. Esto
# quiere decir que la variable c queda encerrada dentro de la funcion y nunca es
# asignada globalmente. Solucion: agregar return c

def suma(a,b):
    c = a + b
    return c
    
a = 2
b = 3
c = suma(a,b)
print(f"La suma da {a} + {b} = {c}")

#%%
# import csv
# from pprint import pprint

# def leer_camion(nombre_archivo):
#     camion=[]
#     registro={}
#     with open(nombre_archivo,"rt") as f:
#         filas = csv.reader(f)
#         encabezado = next(filas)
#         for fila in filas:
#             registro[encabezado[0]] = fila[0]
#             registro[encabezado[1]] = int(fila[1])
#             registro[encabezado[2]] = float(fila[2])
#             camion.append(registro)
#     return camion

# camion = leer_camion("Data/camion.csv")
# pprint(camion)

#entiendo que al appendear un diccionario adentro de una lista va a reemplazar
# todos los nombres de las claves, pero no sé como solucionarlo
