# hipoteca.py
# Archivo de ejemplo
# Ejercicio de hipoteca
saldo=500000.0
tasa=0.05
pago_mensual=2684.11
total_pagado=0.0
mes=0
pago_extra_mes_comienzo=int(input("Ingrese el mes del primer adelanto: "))
pago_extra_mes_fin= pago_extra_mes_comienzo + int(input("Ingrese los meses que desea pagar el adelanto: "))-1
adelanto=int(input("Ingrese el valor del adelanto: "))
while saldo>0:
 #   print (mes, round(total_pagado, 2), round(saldo, 2))
    if mes>=pago_extra_mes_comienzo and mes<pago_extra_mes_fin:
        saldo=saldo*(1+tasa/12)-pago_mensual-adelanto
        total_pagado=total_pagado + pago_mensual+adelanto
        mes+=1
    else:
        if saldo>=pago_mensual:
            saldo=saldo*(1+tasa/12) - pago_mensual
            total_pagado=total_pagado+pago_mensual
            mes+=1
        else:
            total_pagado=total_pagado+saldo
            saldo=0
    print (f"{mes:3} {total_pagado:10.2f} {saldo:10.2f}")
print(f"El total pagado es ${total_pagado:0.2f} en {mes} meses.")
#print("El total pagado es", round(total_pagado,2), "en", mes, "meses.")
