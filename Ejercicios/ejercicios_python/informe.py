import csv
def leer_camion(archivo_camion):
    camion = []
    with open(archivo_camion , "rt") as f:
        lineas = csv.reader(f)
        encabezado = next(lineas)
        for n_item, item in enumerate(lineas, start = 1):
            record = dict(zip(encabezado, item))
            try:
                cant = int(record['cajones'])
                precio = float(record['precio'])
                camion.append({"nombre":record['nombre'], "cajones":cant, "precio":precio})
            except ValueError:
                print(f"{n_item}: No pude interpretar: {item}")
        return camion
def leer_precios(archivo_precios):
    precios = {}
    with open(archivo_precios, "r") as p:
        lineas = csv.reader(p)
        for linea in lineas:
            if len(linea) > 1:
                precios[linea[0]] = float(linea[1])
    return precios
camion = leer_camion("Data/fecha_camion.csv")
precios = leer_precios("Data/precios.csv")
gasto = 0
ganancia = 0
balance = 0
for producto in camion:
    precio_compra = producto["precio"] * producto["cajones"]
    precio_venta = precios[producto["nombre"]] * producto["cajones"]
    gasto += precio_compra
    ganancia += precio_venta
    balance += ganancia - gasto
    # print (precios[producto["nombre"]])
print (f'El gasto del camión fue de ${gasto:0.2f},la venta total fue de'
       f' ${ganancia:0.2f} obteniendose un balance total de ${balance:0.2f}'
       )

if __name__ == '__main__':
    import sys
    if len(sys.argv) != 3:
        raise SystemExit(f'Uso adecuado: {sys.argv[0]} ' 'archivo_camion ' 'archivo_precios')
    camion = leer_camion(sys.argv[1])
    precios = leer_precios(sys.argv[2])

        

