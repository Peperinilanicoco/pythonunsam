#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 18:42:04 2020

@author: mauro
"""
import numpy as np
import matplotlib.pyplot as plt

def randomwalk(largo):
    pasos=np.random.randint (-1,2,largo) 
    return pasos.cumsum()

N = 100000 
#6.10 - item 3
#Nota: para cada random walk, tomo el promedio, y me quedo con el mas 
#cercano/lejano a cero, para definir "la que mas se aleja" o "la que menos se aleja". 
nroRandomWalks = 12
puntos = [] 

indices = [i for i in range(nroRandomWalks)]
for i in range(nroRandomWalks):
    puntos.append(randomwalk(N))

#Obtengo un dicc con todos los resultados
resultados = dict(zip(indices, puntos))

# medias una tupla con (media de la trayectoria, nro de la simulacion)
# nota:  estoy tomando el valor abosluto de la media porque quiero el que mas se aleja hacia los + y hacia los -
#luego, me quedo con el valor maximo y minimo de las medias para tener el que mas se aleja de 0 y el mas cercano a 0
medias = [(abs(np.mean(resultados[i])), i)for i in range(nroRandomWalks)] 
lejos=max(medias)[1] #La trayectoria que mas se aleja, en promedio.
cerca=min(medias)[1] #La trayectoria que menos se aleja, en promedio.


#El plot completo
plt.figure(figsize=(14, 10), dpi=80)
plt.subplot(2, 1, 1) 
for i in range(nroRandomWalks):
    if i==lejos:
        plt.plot(resultados[i], linewidth=2, color="blue", alpha=0.6) # dibuja la curva mas lejana al 0
    elif i==cerca:
        plt.plot(resultados[i], linewidth=2, color="red", alpha=0.6) # dibuja la curva mas cercana al 0
    else:
        plt.plot(resultados[i], linewidth=0.4, alpha=0.6) # dibuja toas las otras curvas
    # plt.yticks(np.linspace(-500,500,3), fontsize=10)
    plt.xticks (np.linspace(0, N, 3), fontsize = 10)
    plt.title(f"Simulacion de {nroRandomWalks} caminatas al azar" , fontsize=16)

    plt.ylabel('Distancia al origen', fontsize=12)
 
plt.subplot(2, 2, 3) 
plt.plot(resultados[lejos], color="blue", linewidth=2.5, linestyle="-")
plt.title("La caminata que más se aleja", fontsize=16)
plt.xticks (np.linspace(0, N, 3), fontsize = 10)
# plt.yticks(np.linspace(-500,500,10), fontsize=10) #Ejes
plt.ylabel('Distancia al origen', fontsize=12)

plt.subplot(2, 2, 4) 
plt.plot(resultados[cerca], color="red", linewidth=2.5, linestyle="-")
plt.xticks (np.linspace(0, N, 3), fontsize = 10)
# plt.yticks(np.linspace(-500,500,10), fontsize=12)
plt.title("La caminata que menos se aleja", fontsize=16)
plt.ylabel('Distancia al origen', fontsize=12)
plt.show()