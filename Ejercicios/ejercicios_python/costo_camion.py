import csv
def costo_camion(nombre_archivo):
    with open(nombre_archivo , "rt") as f:
        lineas = csv.reader(f)
        encabezado = next(lineas)
        # print (encabezado)
        precio_total = 0
        for n_item, item in enumerate(lineas, start = 1):
            record = dict(zip(encabezado, item))
            # print(record)
            try:
                cant = int(record['cajones'])
                precio = float(record['precio'])
                precio_total = precio_total + cant*precio
            except ValueError:
                print(f"{n_item}: No pude interpretar: {item}")
        return precio_total
        

def leer_camion(archivo_camion):
    camion = []
    with open(archivo_camion , "rt") as f:
        lineas = csv.reader(f)
        encabezado = next(lineas)
        for n_item, item in enumerate(lineas, start = 1):
            record = dict(zip(encabezado, item))
            try:
                cant = int(record['cajones'])
                precio = float(record['precio'])
                camion.append({"nombre":record['nombre'], "cajones":cant, "precio":precio})
            except ValueError:
                print(f"{n_item}: No pude interpretar: {item}")
        return camion