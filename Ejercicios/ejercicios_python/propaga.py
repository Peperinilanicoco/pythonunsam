#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 17:54:51 2020

@author: mauro
"""

'''Ejercicio 3.9: Propagación
Imaginate una fila con varios fósforos uno al lado del otro. Los fósforos 
pueden estar en tres estados: nuevos, prendidos fuego o ya gastados 
(carbonizados). Representaremos esta situación con una lista L con un 
elemento por fósforo, que en cada posición tiene un 0 (nuevo), un 1 
(encendido) o un -1 (carbonizado). El fuego se propaga inmediatamente de 
un fósforo encendido a cualquier fósoforo nuevo que tenga a su lado. 
Los fósforos carbonizados no se encienden nuevamente.

Escribí una función llamada propagar que reciba un vector con 0's, 1's y 
-1's y devuelva un vector en el que los 1's se propagaron a sus vecinos con 0.
 Guardalo en un archivo propaga.py.'''

def invertir_lista(lista):   #Voy a usar la funcion del ejercicio anterior
    invertida = []
    i = len(lista)-1
    while i >= 0:
        invertida.append(lista[i])
        i -= 1
    return invertida
def propagar(lista): #Defino la funcion
#empiezo desde la posicion 0
    i = 0
    while i < len(lista):
        if lista[i] == 1:
#Si en la posicion 'i' existe un fosforo encendido (1), guardo esa posición
#en la variable 'pos'
            pos = i             
#recorro todos los valores desde la posicion q tenia el fosforo encendido hasta el final.
#seteo el enumerate para que la posicion inicial del for coincida con la
#posicion original del primer fosforo encendido en la lista de entrada
            for p,k in enumerate(lista[pos:], start = pos):
                if lista[p] == 0:
    #transformo todos los 0 después del 1
                    pos = p
                    lista[pos] = 1
                elif lista[p] == -1:
#si encuentro un fosforo quemado (-1), rompo el ciclo y avanzo un valor en
#la lista a partir de la posicion que estaba ese fosforo
                    pos = p
                    i = pos
                    break
            i = pos+1
        else:
            i += 1
#listo, como mi codigo solo corre hacia la derecha, tengo q invertir la
#salida para que prenda los fosforos apagados (0) a la izquierda de los 
#fosforos encendidos
    inv = invertir_lista(lista)
#reseteo i y repito el paso anterior
    i = 0
    while i < len(inv):
        if inv[i] == 1:
            pos = i
            for p,k in enumerate(inv[pos:], start = pos):
                if inv[p] == 0:
                    pos = p
                    inv[pos] = 1
                elif inv[p] == -1:
                    pos = p
                    i = pos
                    break
            i = pos+1
        else:
            i += 1
#vuelvo a invertir la lista y la asocio a una variable de salida
    salida = invertir_lista(inv)
    return salida