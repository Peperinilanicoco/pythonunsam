#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 12:42:25 2020

@author: mauro
"""
# Ejercicio 7.1: Segundos vividos
# Escribí una función a la que le pasás tu fecha de nacimiento como cadena en 
# formato 'dd/mm/AAAA' (día, mes, año con 2, 2 y 4 dígitos, separados con barras
# normales) y te devuelve la cantidad de segundos que viviste (asumiendo que 
# naciste a las 00:00hs de tu fecha de nacimiento).
# Guardá este código en el archivo vida.py.

from datetime import datetime


def segundos_vividos(fecha):
    fecha_actual = datetime.now()
    nacimiento = datetime.strptime(fecha, '%d/%m/%Y')
    print(nacimiento)
    delta = fecha_actual - nacimiento
    return delta.total_seconds()
    