#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 20:03:52 2020

@author: mauro
"""
# Vamos a trabajar ahora con el archivo 'arbolado-publico-lineal-2017-2018.csv'.
#  Descargalo y guardalo en tu directorio 'Data/'.

# Levantalo y armá un DataFrame df_lineal que tenga solamente las siguiente
#  columnas:['nombre_cientifico', 'ancho_acera', 'diametro_altura_pecho', 
            # 'altura_arbol']
import pandas as pd
import seaborn as sns

# Abrí ambos datasets a los que llamaremos df_parques y df_veredas.
df_veredas = pd.read_csv('Data/arbolado-publico-lineal-2017-2018.csv', low_memory=False)
df_parques = pd.read_csv('Data/arbolado-en-espacios-verdes.csv', low_memory=False)

# Para cada dataset armate otro seleccionando solamente las filas 
# correspondientes a las tipas (llamalos df_tipas_parques y df_tipas_veredas, 
# respectivamente) y las columnas correspondientes
# al diametro a la altura del pecho y alturas. Hacelo como copias 
# (usando .copy() como hicimos más arriba) para poder trabajar en estos nuevos 
# dataframes sin modificar los dataframes grandes originales. Renombrá las 
# columnas que muestran la altura y el diámetro a la altura del pecho para que 
# se llamen igual en ambos dataframes, para ello explorá el comando rename.


cols_sel_parques = ['nombre_cie', 'altura_tot', 'diametro']
cols_sel_veredas = ['nombre_cientifico', 'altura_arbol', 'diametro_altura_pecho']

nom_corr = ['nombre_cientifico', 'altura_arbol', 'diametro_altura_pecho']
#genero un diccionario con los nombres correctos
nombre_columnas = dict(zip(cols_sel_parques, nom_corr))

especie_parques = ['Tipuana Tipu']
especie_veredas = ['Tipuana tipu']

parque = df_parques[cols_sel_parques]
veredas = df_veredas[cols_sel_veredas]

df_tipas_parques = parque[parque['nombre_cie'].isin(especie_parques)].copy()
df_tipas_veredas = veredas[veredas['nombre_cientifico'].isin(especie_veredas)].copy()
df_tipas_parques = df_tipas_parques.rename(columns = nombre_columnas)

# Agregale a cada dataframe (df_tipas_parques y df_tipas_veredas) una columna 
# llamada 'ambiente' que en un caso valga siempre 'parque' y en el otro caso 
# 'vereda'.

df_tipas_parques['ambiente'] = 'parque'
df_tipas_veredas['ambiente'] = 'vereda'

# Juntá ambos datasets con el comando df_tipas = pd.concat([df_tipas_veredas, 
# df_tipas_parques]). De esta forma tenemos en un mismo dataframe la información
# de las tipas distinguidas por ambiente.

df_tipas = pd.concat([df_tipas_veredas, df_tipas_parques])

# Creá un boxplot para los diámetros a la altura del pecho de la tipas 
# distinguiendo los ambientes

df_tipas.boxplot('diametro_altura_pecho', by = 'ambiente')
