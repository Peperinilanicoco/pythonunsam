#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 18:54:21 2020

@author: mauro
"""

def busqueda_binaria(lista, x, verbose = False):
    '''Búsqueda binaria
    Precondición: la lista está ordenada
    Devuelve -1 si x no está en lista;
    Devuelve p tal que lista[p] == x, si x está en lista
    '''
    if verbose:
        print(f'[DEBUG] izq |der |medio')
    pos = -1 # Inicializo respuesta, el valor no fue encontrado
    izq = 0
    der = len(lista) - 1
    while izq <= der:
        comps = 0
        medio = (izq + der) // 2
        if verbose:
            print(f'[DEBUG] {izq:3d} |{der:>3d} |{medio:3d}')
        if lista[medio] == x:
            comps +=1
            pos = medio     # elemento encontrado!
        if lista[medio] > x:
            comps +=1
            der = medio - 1 # descarto mitad derecha
        else:
            comps +=1               # if lista[medio] < x:
            izq = medio + 1 # descarto mitad izquierda
    return pos, comps

def donde_insertar(lista, x):
    '''Búsqueda binaria
    Precondición: la lista está ordenada
    Devuelve la posición donde x podría insertarse, si x no está en lista;
    Devuelve p tal que lista[p] == x, si x está en lista
    '''
    izq = 0
    der = len(lista) - 1
    while izq <= der:
        medio = (izq + der) // 2
        if lista[medio] == x:
            return medio    # elemento encontrado!
        if lista[medio] > x:
            der = medio - 1 # descarto mitad derecha
        else:               # if lista[medio] < x:
            izq = medio + 1 # descarto mitad izquierda
    if x > lista[medio]:
        medio = medio+1
    return medio

def insertar(l,e):
    posicion = donde_insertar(l, e)
    if l[posicion] == e:
        return posicion
    else:
        mitad1 = l[0:posicion]
        mitad2 = l[posicion:-1]
        mitad1.append(e)
        salida = mitad1 + mitad2
    return salida, f'Valor insertado en la posición {posicion}'
        

a = [1,2,3,4,6,7,8,9]
print (insertar(a, 5))
