# hipoteca.py
# Archivo de ejemplo
# Ejercicio de hipoteca
saldo=500000.0
tasa=0.05
pago_mensual=2684.11
total_pagado=0.0
mes=1
adelanto=1000
while saldo>0:
    while mes<=12: #while para los primeros 12 meses de adelanto
        saldo=saldo*(1+tasa/12)-pago_mensual-adelanto
        total_pagado=total_pagado + pago_mensual+adelanto
        mes+=1
    else:
        saldo=saldo*(1+tasa/12) - pago_mensual
        total_pagado=total_pagado+pago_mensual
        mes+=1
mes -=1 #para corregir el ultimo ciclo del while que me suma 1 mes mas
print("El total pagado es", round(total_pagado,2), "en", mes, "meses.")
