import csv
from informe_funciones import leer_camion
def costo_camion(nombre_archivo):
    camion = leer_camion(nombre_archivo)
    precio_total = 0
    for i in camion:
        cajones = i['cajones']
        precio = i['precio']
        precio_total = precio_total + cajones*precio
    return precio_total

if __name__ == '__main__':
    import sys
    nombre_archivo = sys.argv[1]
