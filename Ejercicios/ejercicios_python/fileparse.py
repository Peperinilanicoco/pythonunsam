#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 19:45:30 2020

@author: mauro
"""
import csv

def parse_csv(lines, select = None, types = [str, int, float]):
    '''
    parsea un archivo CSV en una lista de registros
    '''
    filas = csv.reader(lines)
        #lee los encabezados
    encabezados = next(filas)
    if select:
        indices = [encabezados.index(ncolumna) for ncolumna in select]
        encabezados = select
    else:
        indices = []
    registros = []
    for fila in filas:
        if not fila: #saltea filas sin datos
            continue
            #filtrar la fila si se especificaron columnas
        if indices:
            if types:
                fila = [func(val) for func, val in zip(types, fila)]
            else:
                fila = [fila[index] for index in indices]
            #armar el diccionario
        registro = dict(zip(encabezados, filas))
        registros.append(registro)
    return registros
