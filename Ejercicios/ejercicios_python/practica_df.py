#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 20:03:52 2020

@author: mauro
"""
# Vamos a trabajar ahora con el archivo 'arbolado-publico-lineal-2017-2018.csv'.
#  Descargalo y guardalo en tu directorio 'Data/'.

# Levantalo y armá un DataFrame df_lineal que tenga solamente las siguiente
#  columnas:['nombre_cientifico', 'ancho_acera', 'diametro_altura_pecho', 
            # 'altura_arbol']
import pandas as pd
import seaborn as sns

df = pd.read_csv('Data/arbolado-publico-lineal-2017-2018.csv')
cols_sel = ['nombre_cientifico', 'ancho_acera', 'diametro_altura_pecho', 
            'altura_arbol']
df_lineal = df[cols_sel]

# Imprimí las diez especies más frecuentes con sus respectivas cantidades.

print(df_lineal['nombre_cientifico'].value_counts().head(10))

especies_seleccionadas = ['Tilia x moltkei', 'Jacaranda mimosifolia', 
                          'Tipuana tipu']
df_lineal_seleccion = df_lineal[df_lineal['nombre_cientifico']
                                .isin(especies_seleccionadas)]

# El siguiente comando realiza un boxplot de los diámetros de los árboles 
# agrupados por especie.

df_lineal_seleccion.boxplot('diametro_altura_pecho', by = 'nombre_cientifico')

# Realizá un gráfico similar pero de los altos en lugar de los diámetros de los
# árboles.

df_lineal_seleccion.boxplot('altura_arbol', by = 'nombre_cientifico')

sns.pairplot(data = df_lineal_seleccion[cols_sel], hue = 'nombre_cientifico')