#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 20:01:48 2020

@author: mauro
"""

import random
import matplotlib.pyplot as plt
import numpy as np

def busqueda_binaria(lista, x):
    '''Búsqueda binaria
    Precondición: la lista está ordenada
    Devuelve -1 si x no está en lista;
    Devuelve p tal que lista[p] == x, si x está en lista
    '''
    pos = -1 # Inicializo respuesta, el valor no fue encontrado
    izq = 0
    der = len(lista) - 1
    comps = 0
    while izq <= der:
        medio = (izq + der) // 2
        if lista[medio] == x:
            comps +=1
            pos = medio     # elemento encontrado!
        if lista[medio] > x:
            comps +=1
            der = medio - 1 # descarto mitad derecha
        else:
            comps +=1               # if lista[medio] < x:
            izq = medio + 1 # descarto mitad izquierda
    return pos, comps
def busqueda_secuencial_(lista,e):
    '''Si e está en la lista devuelve el índice de su primer aparición, 
    de lo contrario devuelve -1.
    '''
    comps = 0 #inicializo en cero la cantidad de comparaciones
    pos = -1
    for i,z in enumerate(lista):
        comps += 1 #sumo la comparación que estoy por hacer
        if z == e:
            pos = i
            break
    return pos, comps
def generar_lista(n,m):
    l = random.sample(range(m),k=n)
    l.sort()
    return l

def generar_elemento(m):
    return random.randint(0,m-1)


def experimento_secuencial_promedio(lista,m,k):
    comps_tot = 0
    for i in range(k):
        e = generar_elemento(m)
        comps_tot += busqueda_secuencial_(lista,e)[1]

    comps_prom = comps_tot / k
    return comps_prom

#Usando experimento_secuencial_promedio(lista,m,k) como base, escribí 
#una función experimento_binario_promedio(lista,m,k) que cuente la cantidad
# de comparaciones que realiza en promedio (entre k experimentos 
#elementales) la búsqueda binaria sobre la lista pasada como parámetro.
def experimento_binario_promedio(lista,m,k):
    comps_tot = 0
    for i in range(k):
        e = generar_elemento(m)
        comps_tot += busqueda_binaria(lista,e)[1]

    comps_prom = comps_tot / k
    return comps_prom

m = 10000
k = 1000

largos = np.arange(256)+1 #estos son los largos de listas que voy a usar
comps_promedio_sec = np.zeros(256) #aca guardo el promedio de comparaciones sobre una lista de largo i, para i entre 1 y 256.
comps_promedio_bin = np.zeros(256)
for i, n in enumerate(largos):
    lista = generar_lista(n,m) # genero lista de largo n
    comps_promedio_sec[i] = experimento_secuencial_promedio(lista,m,k)
    comps_promedio_bin[i] = experimento_binario_promedio(lista, m, k)

#ahora grafico largos de listas contra operaciones promedio de búsqueda.
plt.plot(largos,comps_promedio_sec,label='Búsqueda Secuencial')
plt.plot(largos, comps_promedio_bin, label = 'Búsqueda Binaria')
plt.xlabel("Largo de la lista")
plt.ylabel("Cantidad de comparaciones")
plt.title("Complejidad de la Búsqueda")
plt.legend()