#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 16:42:05 2020

@author: mauro
"""

'''En este primer ejercicio tenés que escribir una función buscar_u_elemento()
 que reciba una lista y un elemento y devuelva la posición de la última
 aparición de ese elemento en la lista (o -1 si el elemento no pertenece 
  a la lista). Probá tu función con algunos ejemplos:'''
 
def buscar_u_elemento(lista, u):
    posicion = -1
    for i, n in enumerate(lista):
        # print (n)
        if n == u:
            posicion = i
    return posicion

'''Agregale a tu programa busqueda_en_listas.py una función 
buscar_n_elemento() que reciba una lista y un elemento y devuelva la cantidad 
de veces que aparece el elemento en la lista. Probá también esta función con 
algunos ejemplos.'''

def buscar_n_elemento(lista, n):
    rep = 0
    for i in lista:
        if i == n:
            rep += 1
    return rep

'''Ejercicio 3.7: Búsqueda de máximo y mínimo
Agregale a tu archivo busqueda_en_listas.py una función maximo() que busque 
el valor máximo de una lista de números positivos.'''

def maximo(lista):
    if lista[0] < 0:
        m = lista[0]
    else:
        m = 0
    for i in lista:
        if i > m:
            m = i
    return m

'''Si te dan ganas, agregá una función minimo() al archivo.'''
def minimo(lista):
    if lista[0] > 0:
        mi = lista[0]
    else:
        mi = 0
    for i in lista:
        if i < mi:
            mi = i
    return mi

