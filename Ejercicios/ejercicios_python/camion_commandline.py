import csv
import sys
def costo_camion(nombre_archivo):
    with open(nombre_archivo , "rt") as f:
        lineas = csv.reader(f)
        precio_total = 0
        encabezado = next(lineas)
        for item in lineas:
            try:
                cant = int(item[1])
                precio = float(item[2])
                precio_total = precio_total + cant*precio
            except ValueError:
                print(f"La información del producto {item[0]} está incompleta o es incorrecta. Por favor, revise el archivo '{nombre_archivo}'")
        return precio_total
if len(sys.argv) == 2:
    nombre_archivo = sys.argv[1]
else:
    nombre_archivo = "Data/camion.csv"

costo = costo_camion(nombre_archivo)
print("Costo total:", costo)
