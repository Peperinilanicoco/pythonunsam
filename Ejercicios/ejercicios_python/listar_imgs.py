#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 19:23:37 2020

@author: mauro
"""

import os

def busca_png(directorio):
    salida = []
    for root, carpeta, archivo in os.walk(directorio):
        for imagen in archivo:
            if imagen.endswith('.png'):
                salida.append(imagen)
    return salida

      
if __name__ == '__main__':
    import sys
    pngs = busca_png(sys.argv[1])
    for i in pngs:
        print (i)