#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 22:06:52 2020

@author: mauro
"""
import csv
def leer_parque(nombre_archivo, parque):
    with open(nombre_archivo, 'rt') as f:
        lineas = csv.reader(f)
        encabezado = next(lineas)
        for n_item, item in enumerate(lineas, start = 1):
            encabezado = 'espacio_ve'[parque]
            record = dict(zip(encabezado, item))
            # print(record)
            return record
parque = leer_parque('Data/arbolado-en-espacios-verdes.csv','AVELLANEDA')
