#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 22:28:58 2020

@author: mauro ramirez
"""
import csv
def leer_camion(archivo_camion):
    camion = []
    with open(archivo_camion , "rt") as f:
        lineas = csv.reader(f)
        encabezado = next(lineas)
        for n_item, item in enumerate(lineas, start = 1):
            record = dict(zip(encabezado, item))
            try:
                cant = int(record['cajones'])
                precio = float(record['precio'])
                camion.append({"nombre":record['nombre'], "cajones":cant, "precio":precio})
            except ValueError:
                print(f"{n_item}: No pude interpretar: {item}")
        return camion
def leer_precios(archivo_precios):
    precios = {}
    with open(archivo_precios, "r") as p:
        lineas = csv.reader(p)
        for linea in lineas:
            if len(linea) > 1:
                precios[linea[0]] = float(linea[1])
    return precios
def hacer_informe(camion, precios):
    informe = []
    for row in camion:
        producto = row['nombre']
        cajones = row['cajones']
        precio_compra = row['precio']
        precio_venta = precios[row['nombre']]
        cambio = precio_venta - precio_compra
        informe.append((producto, cajones, precio_compra, cambio))
    return informe
camion = leer_camion("Data/fecha_camion.csv")
# print ('c', camion)
precios = leer_precios("Data/precios.csv")
# print('p', precios)
informe = hacer_informe(camion, precios)
headers = ('Nombre', 'Cajones', 'Precio', 'Cambio')
print(f"{headers[0]:>10s} {headers[1]:>10s} {headers[2]:>10s} {headers[3]:>10}")
print('---------- ---------- ---------- ----------')
for nombre, cajones, precio, cambio in informe:
    print(f'{nombre:>10s} {cajones:>10d} {precio:>10.2f} {cambio:>10.2f}')
