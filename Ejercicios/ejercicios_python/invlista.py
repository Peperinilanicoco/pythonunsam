#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 17:35:27 2020

@author: mauro
"""

'''Ejercicio 3.8: Invertir una lista
Escribí una función invertir_lista(lista) que dada una lista devuelva
 otra que tenga los mismos elementos pero en el orden inverso. Es decir, 
 el que era el primer elemento de la lista de entrada deberá ser el último 
 de la lista de salida y análogamente con los demás elementos.'''
 
def invertir_lista(lista):
    invertida = []
    i = len(lista)-1
    while i >= 0:
        invertida.append(lista[i])
        i -= 1
    return invertida

        
     